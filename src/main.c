#include "test.h"

int main() {
    if (run_all_tests()) {
        printf("Tests have passed!\n");
        return 0;
    }
    
    printf("Tests have failed\n");
    return 1;
}

// Pipeline work
